import time
import unittest
import string
import random

from selenium import webdriver

class TestLaravel(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome('C:\PYTON TEST\chromedriver.exe')
        self.driver.get('https://laravel.dev-society.com/')

    def tearDown(self):
        self.driver.quit()

    # afisare paginaRegister - click
    def test_a_Register(self):
        register_page = self.driver.find_element_by_xpath('//*[@id="register_button"]')
        register_page.click()
        time.sleep(3)
        self.assertIn('register', self.driver.current_url)

    # afisare pagina Login - click
    def test_b_Login(self):
        login_page = self.driver.find_element_by_xpath('//*[@id="login_button"]')
        login_page.click()
        time.sleep(3)
        self.assertIn('login', self.driver.current_url)

    # testare Login user si parola
    def login(self, user, password):
        login_button = self.driver.find_element_by_xpath('//*[@id="login_button"]')
        login_button.click()
        time.sleep(3)

        email_box = self.driver.find_element_by_xpath('//*[@id="email"]')
        password_button = self.driver.find_element_by_xpath(('//*[@id="password"]'))

        email_box.clear()
        password_button.clear()

        email_box.send_keys(user)
        password_button.send_keys(password)

        login_button = self.driver.find_element_by_xpath('//*[@id="login_user"]')
        login_button.click()
        time.sleep(3)

    def test_c_login(self):
        self.login('dianatest@email.com', 'Dianatest')

    def test_d_login_invaliduser(self):
        self.login('diana@gmail.com','Dianatest')
        self.assertIn('These credentials do not match our records.', self.driver.page_source)

    def test_e_login_invalidpass(self):
        self.login('dianatest@email.com','Diana')
        self.assertIn('These credentials do not match our records.', self.driver.page_source)

    # testare buton Register

    def test_f_register(self):
        register_button = self.driver.find_element_by_xpath('//*[@id="register_button"]')
        register_button.click()
        time.sleep(3)
        self.assertIn('Register', self.driver.page_source)

        username_box = self.driver.find_element_by_xpath('//*[@id="name"]')
        email_box = self.driver.find_element_by_xpath('//*[@id="email"]')

        username_box.clear()
        email_box.clear()

        random_email = ''.join(random.choices(string.ascii_lowercase, k=10)) + '@email.com'
        random_username = ''.join(random.choices(string.ascii_lowercase, k=10)) + '_' + str(
            random.randint(100000, 999999))

        email_box.send_keys(random_email)
        username_box.send_keys(random_username)
        time.sleep(3)

        password = random.choices(string.ascii_lowercase, k=10)
        pass_button = self.driver.find_element_by_xpath('//*[@id="password"]')
        pass_button.send_keys(password)
        confirm_pass_button = self.driver.find_element_by_xpath('//*[@id="password-confirm"]')
        confirm_pass_button.send_keys(password)

        register_button = self.driver.find_element_by_xpath('//*[@id="register_account"]')
        register_button.click()

        time.sleep(3)
        self.assertIn('You have no posts yet ', self.driver.page_source)

    # Testare add new post

    def test_g_Addpost(self):
        self.login('dianatest@email.com', 'Dianatest')
        post_button = self.driver.find_element_by_xpath('//*[@id="sidebar-posts"]/a')
        post_button.click()
        self.assertIn('post', self.driver.current_url)
        time.sleep(2)
        addanewpost_button = self.driver.find_element_by_xpath('//*[@id="create_post"]')
        addanewpost_button.click()
        time.sleep(1)
        self.assertIn('create', self.driver.current_url)
        title_box = self.driver.find_element_by_xpath('/html/body/main/div/div/main/form/input[2]')
        title_box.send_keys('test 2')
        body_box = self.driver.find_element_by_xpath('//*[@id="text_editor"]')
        body_box.send_keys('Testare add new post diana')
        time.sleep(3)
        submit_button = self.driver.find_element_by_xpath('//*[@id="submit_post"]')
        submit_button.click()
        time.sleep(2)
        self.assertIn('Post created', self.driver.page_source)
        a = str(self.driver.current_url)
        lista = a.split('/')
        TestLaravel.nrid = lista[len(lista) - 1]

        TestLaravel.xpath_edit = '//*[@id="edit-post-' + TestLaravel.nrid + '"]'
        TestLaravel.xpath_delete = '//*[@id="delete-post-' + TestLaravel.nrid + '"]'

    # testare edit post
    def test_h_Edit(self):
        self.login('dianatest@email.com', 'Dianatest')
        edit_button = self.driver.find_element_by_xpath(TestLaravel.xpath_edit)
        edit_button.click()
        time.sleep(3)
        titlu_modif = self.driver.find_element_by_xpath('/html/body/main/div/div/main/form/input[2]')
        titlu_modif.clear()
        titlu_modif.send_keys('test nou diana')
        time.sleep(3)
        submit_button = self.driver.find_element_by_xpath('//*[@id="submit_post"]')
        submit_button.click()

    #testare delete post
    def test_i_DeletePost(self):
        self.login('dianatest@email.com', 'Dianatest')
        DeleteButton = self.driver.find_element_by_xpath(TestLaravel.xpath_delete)
        DeleteButton.click()
        time.sleep(3)

    # testare edit user
    def test_j_modificare_settings(self):
        self.login('dianatest@email.com', 'Dianatest')
        settings_button = self.driver.find_element_by_xpath('//*[@id="sidebar-settings"]/a')
        settings_button.click()
        edit_buton = self.driver.find_element_by_xpath('//*[@id="edit_account"]')
        edit_buton.click()
        time.sleep(3)
        user_name_button = self.driver.find_element_by_xpath('//*[@id="edit_name"]')
        user_name_button.clear()
        random_username = ''.join(random.choices(string.ascii_lowercase, k=10)) + '_' + str(
            random.randint(100000, 999999))
        user_name_button.send_keys(random_username)
        time.sleep(3)
        submit_button = self.driver.find_element_by_xpath('//*[@id="apply"]')
        submit_button.click()
        self.assertIn('User updated', self.driver.page_source)


# testare edit other user
    def test_k_modificare_settings(self):
        self.login('dianatest@email.com', 'Dianatest')
        users_button = self.driver.find_element_by_xpath('//*[@id="sidebar-users"]/a')
        users_button.click()
        edit_buton = self.driver.find_element_by_xpath('//*[@id="edit-user-41"]')
        edit_buton.click()
        time.sleep(3)
        user_name_button = self.driver.find_element_by_xpath('//*[@id="edit_name"]')
        user_name_button.clear()
        random_username = ''.join(random.choices(string.ascii_lowercase, k=10)) + '_' + str(
            random.randint(100000, 999999))
        user_name_button.send_keys(random_username)
        time.sleep(3)
        submit_button = self.driver.find_element_by_xpath('//*[@id="apply"]')
        submit_button.click()
        self.assertIn('User updated', self.driver.page_source)

    # testare delete user
    def test_l_delete_user(self):
        register_button = self.driver.find_element_by_xpath('//*[@id="register_button"]')
        register_button.click()
        time.sleep(3)
        self.assertIn('Register', self.driver.page_source)

        username_box = self.driver.find_element_by_xpath('//*[@id="name"]')
        email_box = self.driver.find_element_by_xpath('//*[@id="email"]')

        username_box.clear()
        email_box.clear()

        email_box.send_keys('dianatest2@email.com')
        username_box.send_keys('dianatest2')
        time.sleep(3)

        password = ('Dianatest2')
        pass_button = self.driver.find_element_by_xpath('//*[@id="password"]')
        pass_button.send_keys(password)
        confirm_pass_button = self.driver.find_element_by_xpath('//*[@id="password-confirm"]')
        confirm_pass_button.send_keys(password)

        register_button = self.driver.find_element_by_xpath('//*[@id="register_account"]')
        register_button.click()

        time.sleep(3)
        self.assertIn('You have no posts yet ', self.driver.page_source)

        users_setting = self.driver.find_element_by_xpath('//*[@id="sidebar-settings"]/a')
        users_setting.click()
        delete_user_button = self.driver.find_element_by_xpath('//*[@id="delete_button"]')
        delete_user_button.click()
        time.sleep(3)
        self.assertIn('Laravel', self.driver.page_source)


